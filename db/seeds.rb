# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Feature.destroy_all

features = ["CHARMING PERIOD RESIDENCE C1900- EDWARDIAN ERA",
            "PLETHORA OF APPEALING ORIGINAL FEATURES",
            "ELEMENT OF MODERN ADDITIONS",
            "FOUR RECEPTION ROOMS",
            "KITCHEN WITH BREAKFAST AREAS",
            "FIVE DOUBLE BEDROOMS & TWO BATHROOMS",
            "LIGHT & SUNNY AMBIENCE",
            "GROUNDS APPROX 0.5 ACRE",
            "GARAGE & PARKING FOR MANY VEHICLES",
            "SEXEY'S SCHOOL CATCHMENT AREA",
            "LIVING ROOM",
            "DINING ROOM/SUN LOUNGE",
            "FITTED KITCHEN",
            "DOWNSTAIRS CLOAKROOM",
            "BATHROOM",
            "TIMBER FRAMED DOUBLE GLAZED WINDOWS",
            "GAS CENTRAL HEATING",
            "DOUBLE GARAGE",
            "GARDENS",
            "FOUR DOUBLE BEDROOMS",
            "FITTTED KITCHEN & UTILITY ROOM",
            "BREAKFAST ROOM",
            "SITTING ROOM & DINING ROOM",
            "DOWNSTAIRS CLOAKROOM",
            "BATHROOM",
            "ENSUITE SHOWER ROOM & ENSUITE BATHROOM",
            "DOUBLE GARAGE",
            "FRONT AND REAR GARDENS",
            "GAS CENTRAL HEATING",
            "NO UPWARD CHAIN – VACANT POSSESSION UPON REQUEST"
          ]

features.each do |f|
  Feature.create(title: f)
end

30.times do
  address = "#{Faker::Address.building_number}, #{Faker::Address.secondary_address}, #{Faker::Address.city}"
  property = Property.new(address: address, price: Faker::Number.decimal(6), description: Faker::ChuckNorris.fact)

  sample_features = features.sample(features.count)
  key_feature = property.key_features.build

  (rand(4)+1).times do
    photo = Photo.new
    property.photos << photo
  end

  (rand(2)+1).times do
    photo = FloorPlan.new
    property.floor_plans << photo
  end

  sample_features.each do |sf|
    feature = Feature.find_by(title: sf)
    feature.key_features << key_feature
  end
end


