class AddImagesToFloorPlans < ActiveRecord::Migration[5.0]
  def up
    add_attachment :floor_plans, :image
  end

  def down
    remove_attachment :floor_plans, :image
  end
end
