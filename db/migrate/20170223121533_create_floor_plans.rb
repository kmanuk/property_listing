class CreateFloorPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :floor_plans do |t|
      t.belongs_to :property
      t.timestamps
    end
  end
end
