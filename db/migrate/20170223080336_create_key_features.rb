class CreateKeyFeatures < ActiveRecord::Migration[5.0]
  def change
    create_table :key_features do |t|
      t.belongs_to :feature
      t.belongs_to :property

      t.timestamps
    end
  end
end
