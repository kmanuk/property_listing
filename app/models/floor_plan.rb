class FloorPlan < ApplicationRecord
  belongs_to :property

  paperclip_opts = {
    :styles => { :thumb => '40x40#', :medium => '150x200>', :large => '300x300>' },
    :convert_options => { :all => '-quality 92' },
    :processor       => [ :cropper ]
  }

  unless Rails.env.development?
    paperclip_opts.merge! :storage        => :s3,
                          :s3_credentials => "#{RAILS_ROOT}/config/s3.yml",
                          :path           => ':attachment/:id/:style.:extension',
                          :bucket         => 'property_uploads'
  end

  has_attached_file :image, paperclip_opts
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
