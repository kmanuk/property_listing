class Feature < ApplicationRecord

  has_many :key_features, dependent: :destroy
  has_many :properties, through: :key_features

end
