class Property < ApplicationRecord

  has_many :key_features, dependent: :destroy
  has_many :features, through: :key_features
  has_many :photos, dependent: :destroy
  has_many :floor_plans, dependent: :destroy

  validates :address, presence: true

  def add_photos(photo_params)
    photos = photo_params[:image].map { |img| self.photos.build(Hash[:image => img]) } unless photo_params.empty?
    self.photos << photos unless photos.nil?
  end

  def add_floor_plans(floor_plan_params)
    floor_plans = floor_plan_params[:image]
      .map { |img| self.floor_plans.build(Hash[:image => img]) } unless floor_plan_params.empty?
    self.floor_plans << floor_plans unless floor_plans.nil?
  end

  def add_key_features(key_feature_params)
    key_features = key_feature_params[:key_features]
    key_features.shift

    key_features.each do |kf|
      feature = Feature.find(kf)
      key_feature = self.key_features.build
      feature.key_features << key_feature
    end
  end
end
