class PropertiesController < ApplicationController
  def index
    @properties = Property.order(:id).page params[:page]
  end

  def show
    @property = Property.find(params[:id])
  end

  def new
    @photo = Photo.new
    @floor_plan = FloorPlan.new
    @property = @photo.build_property
  end

  def create
    @property = Property.new(property_params)

    if @property.save
      @property.add_photos(photo_params)
      @property.add_floor_plans(floor_plan_params)
      @property.add_key_features(key_feature_params)

      redirect_to properties_path
    end
  end

  private

  def photo_params
    params.fetch(:photo,{}).permit(:image => [])
  end

  def floor_plan_params
    params.fetch(:floor_plan,{}).permit(:image => [])
  end

  def key_feature_params
    params.fetch(:property,{}).permit(:key_features => [])
  end

  def property_params
    params.fetch(:property).permit(:description, :address, :price)
  end
end
