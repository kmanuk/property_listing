# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Clone from repo
```
git clone git@bitbucket.org:arm2pro/property_listing.git
```
* Setup project

```
cd property_listing
bundle install
```

* Setup DB
```
rake db:setup
```

* To see your real data after uploads please change `Faker::Avatar.image` in follows part of code
places.

```ruby
  <% @property.photos.each do |photo| %>
  <div class="item">
    <%= image_tag photo.image.url, class: 'img-responsive' %>
  </div>
  <% end %>
```
and here
```ruby
  <% @property.floor_plans.each do |photo| %>
    <%= image_tag photo.image.url, class: 'img-responsive' %>
  <% end %>
```
Don't judge me too strong for **fake** data :)
