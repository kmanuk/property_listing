Rails.application.routes.draw do
  get '/auth/:provider/callback', to: 'sessions#create'
  match "/signout" => "sessions#destroy", :as => :signout, via: :delete
  resources :properties

  root 'properties#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
